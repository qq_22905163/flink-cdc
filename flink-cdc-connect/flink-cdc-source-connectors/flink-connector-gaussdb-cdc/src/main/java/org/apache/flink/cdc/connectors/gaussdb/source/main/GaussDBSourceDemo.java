package org.apache.flink.cdc.connectors.gaussdb.source.main;

import org.apache.flink.cdc.connectors.gaussdb.source.GaussDBSourceFunction;
import org.apache.flink.cdc.connectors.gaussdb.source.GaussDBSqlSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class GaussDBSourceDemo {


    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        GaussDBSourceFunction<String> build = GaussDBSqlSource.<String>builder()
                .hostname("")   // 服务器地址
                .port(8001)     // ha端口，默认8001
                .username("")   // 用户名
                .password("")   // 密码
                .database("")   // 库名
                .tableList("")  // 单表：public.tablename 全表：public.* 多表：public.tableA,public.tableB
                .slotName("")   // 复制槽
                .replicationMode("2")    // 逻辑复制槽模式： 1 创建逻辑复制槽 ，2 使用逻辑复制槽 ，3 删除逻辑复制槽 （需先创建再使用）
                .build();
        env.addSource(build).print();

        env.execute();
    }
}
